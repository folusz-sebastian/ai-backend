# Backend for restaurant management system

##  Opis projektu
Repozytorium jest częścią
[projektu](https://bitbucket.org/folusz-sebastian/workspace/projects/RMSA)
będącego aplikacją webową systememu zarządzania restauracją.

Aplikacja została napisana w języku Java przy wykorzystaniu framework-a Spring Boot.

Dodatkowo w aplikacji zostały wykorzystane takie moduły/biblioteki jak:

- spring security
- spring JPA
- spring REST
- biblioteka Jasypt
- google-api-services-calendar
- mapStruct
- Lombok

Aplikacja łączy się z relacyjną bazą danych, której model został przedstawiony w repozytorium ->
[baza danych](https://bitbucket.org/folusz-sebastian/database-for-restaurant-management-system)