package pl.folusz.sebastian.app.dao.dish;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.folusz.sebastian.app.entity.dish.Dish;
import pl.folusz.sebastian.app.entity.dish.DishIngredient;

import java.util.List;

public interface DishIngredientRepository extends JpaRepository<DishIngredient, Long> {
    @Query("from DishIngredient dishIngredient " +
            "where dishIngredient.dish.id = :dishId"
    )
    List<DishIngredient> findByDishId(@Param("dishId") long dishId);
}
