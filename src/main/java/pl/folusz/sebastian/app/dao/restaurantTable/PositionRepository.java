package pl.folusz.sebastian.app.dao.restaurantTable;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.folusz.sebastian.app.entity.restaurantTable.Position;

public interface PositionRepository extends JpaRepository<Position, Long> {
    Position findByXInPercentsAndYInPercents(double xInPercents, double yInPercents);
}
