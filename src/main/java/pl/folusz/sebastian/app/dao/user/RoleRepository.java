package pl.folusz.sebastian.app.dao.user;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.folusz.sebastian.app.entity.user.UserRole;

public interface RoleRepository extends JpaRepository<UserRole, Integer> {
    UserRole findByRole(String role);

}
