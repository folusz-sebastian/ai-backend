package pl.folusz.sebastian.app.dao.dish;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.folusz.sebastian.app.entity.dish.DishCategory;

public interface DishCategoryRepository extends JpaRepository<DishCategory, Long> {
    DishCategory findByName(String name);
}
