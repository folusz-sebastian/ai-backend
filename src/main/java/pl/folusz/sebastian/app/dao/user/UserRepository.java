package pl.folusz.sebastian.app.dao.user;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.folusz.sebastian.app.entity.user.User;


public interface UserRepository extends JpaRepository<User, String> {
    User findByEmail(String email);
    Boolean existsByEmail(String email);
}
