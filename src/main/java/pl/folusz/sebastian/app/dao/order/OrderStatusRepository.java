package pl.folusz.sebastian.app.dao.order;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.folusz.sebastian.app.entity.order.OrderStatus;

public interface OrderStatusRepository extends JpaRepository<OrderStatus, Long> {
    OrderStatus findByName(String name);
    Boolean existsByName(String name);
}
