package pl.folusz.sebastian.app.dao.dish;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.folusz.sebastian.app.entity.dish.Ingredient;
import pl.folusz.sebastian.app.entity.dish.MeasurementUnit;

public interface IngredientRepository extends JpaRepository<Ingredient, Long> {
}
