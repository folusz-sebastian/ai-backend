package pl.folusz.sebastian.app.dao.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.folusz.sebastian.app.entity.order.Order;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    @Query(value =
            "select * from restaurant_order\n" +
                    "   inner join status on restaurant_order.status_id = status.status_id\n" +
                    "   inner join reservation r on restaurant_order.reservation_id = r.reservation_id\n" +
                    "where r.date >= current_date\n" +
                    "and status.name = 'przygotowywane'" +
                    "order by r.date",
            nativeQuery = true
    )
    List<Order> findAllNotPreparedFromToday();
}
