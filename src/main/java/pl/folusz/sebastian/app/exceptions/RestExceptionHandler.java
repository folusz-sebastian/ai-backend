package pl.folusz.sebastian.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<ExceptionModel> handleException(ObjectNotFoundException exception) {
        ExceptionModel exceptionModel = new ExceptionModel();
        exceptionModel.setStatus(HttpStatus.NOT_FOUND.value());
        exceptionModel.setMessage(exception.getMessage());
        exceptionModel.setTimeStamp(System.currentTimeMillis());

        return new ResponseEntity<>(exceptionModel, HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<ExceptionModel> handleException(Exception exception) {
        ExceptionModel exceptionModel = new ExceptionModel();
        exceptionModel.setStatus(HttpStatus.BAD_REQUEST.value());
        exceptionModel.setMessage(exception.getMessage());
        exceptionModel.setTimeStamp(System.currentTimeMillis());

        return new ResponseEntity<>(exceptionModel, HttpStatus.BAD_REQUEST);
    }

}
