package pl.folusz.sebastian.app.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
class ExceptionModel {
    private int status;
    private String message;
    private long timeStamp;
}
