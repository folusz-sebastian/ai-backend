package pl.folusz.sebastian.app.DTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Digits;

@Getter @Setter
public class DishIngredientDto {
    private long id;
    private IngredientDto ingredient;

    @Digits(integer = 5, fraction = 2)
    private double quantity;
}
