package pl.folusz.sebastian.app.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import pl.folusz.sebastian.app.entity.restaurantTable.Position;

@Getter @Setter
public class RestaurantTableDto {
    private long id;

    @JsonProperty("length_in_percents")
    private double lengthInPercents;

    @JsonProperty("width_in_percents")
    private double widthInPercents;

    private Position position;
}
