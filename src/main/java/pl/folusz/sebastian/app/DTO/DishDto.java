package pl.folusz.sebastian.app.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import pl.folusz.sebastian.app.entity.dish.DishCategory;

import javax.validation.constraints.Digits;
import java.util.List;

@Getter @Setter
public class DishDto {
    private long id;
    private String name;

    @Digits(integer = 5, fraction = 2)
    private double price;

    private DishCategory category;

    @JsonProperty("dish_ingredients")
    private List<DishIngredientDto> dishIngredients;
}
