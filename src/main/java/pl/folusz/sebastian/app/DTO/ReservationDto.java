package pl.folusz.sebastian.app.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter @Setter
public class ReservationDto {
    private long id;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date date;
    @JsonProperty("beginning_hour")
    private int beginningHour;
    private int duration;
    private RestaurantTableDto table;
}
