package pl.folusz.sebastian.app.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PositionDto {
    private long id;

    @JsonProperty("x_in_percents")
    private double xInPercents;

    @JsonProperty("y_in_percents")
    private double yInPercents;
}
