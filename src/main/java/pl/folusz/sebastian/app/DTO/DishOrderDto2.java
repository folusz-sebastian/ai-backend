package pl.folusz.sebastian.app.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DishOrderDto2 {
    private long id;
    private DishFromOrderDto dish;
    private int quantity;
    private OrderDto2 order;
}
