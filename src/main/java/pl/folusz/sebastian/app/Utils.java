package pl.folusz.sebastian.app;

import java.util.Date;

public class Utils {
    public static String createDateTimeString(Date date, int beginningHour) {
        return date.toString() + "T" + digitToTwoDigitString(beginningHour) + ":00:00Z";
    }

    private static String digitToTwoDigitString(int digit) {
        return digit < 10 ? "0" + digit : "" + digit;
    }
}
