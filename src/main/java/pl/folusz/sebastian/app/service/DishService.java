package pl.folusz.sebastian.app.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.DTO.DishDto;
import pl.folusz.sebastian.app.DTO.DishIngredientDto;
import pl.folusz.sebastian.app.DTO.IngredientDto;
import pl.folusz.sebastian.app.Mapper.DishIngredientMapper;
import pl.folusz.sebastian.app.Mapper.DishMapper;
import pl.folusz.sebastian.app.Mapper.IngredientMapper;
import pl.folusz.sebastian.app.dao.dish.*;
import pl.folusz.sebastian.app.entity.dish.*;
import pl.folusz.sebastian.app.exceptions.ObjectNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DishService {
    private DishRepository dishRepository;
    private IngredientRepository ingredientRepository;
    private DishIngredientRepository dishIngredientRepository;
    private DishCategoryRepository dishCategoryRepository;

    public DishService(DishRepository dishRepository, IngredientRepository ingredientRepository,
                       DishIngredientRepository dishIngredientRepository, DishCategoryRepository dishCategoryRepository) {
        this.dishRepository = dishRepository;
        this.ingredientRepository = ingredientRepository;
        this.dishIngredientRepository = dishIngredientRepository;
        this.dishCategoryRepository = dishCategoryRepository;
    }

    public DishDto save(Dish dish) {
        dish.setId(0);
        return setDishData(dish);
    }

    public DishDto update(long dishId, Dish dish) {
        Optional<Dish> optionalDish = dishRepository.findById(dishId);
        if (!optionalDish.isPresent()) {
            throw new ObjectNotFoundException("Dish was not found. Id is not correct");
        }
        return setDishData(dish);
    }

    private DishDto setDishData(Dish dish) {
        if (dish.getId() > 0){
            deleteOldDishIngredients(dish);
        }

        List<DishIngredient> oldDishIngredientList = dish.getDishIngredients();
        dish.setDishIngredients(null);

        DishCategory dishCategory = new DishCategory();
        Optional<DishCategory> categoryOptional = dishCategoryRepository.findById(dish.getCategory().getId());
        if (categoryOptional.isPresent()) {
            dishCategory = categoryOptional.get();
        }
        dish.setCategory(dishCategory);

        dish = dishRepository.save(dish);
        DishDto dishDto = DishMapper.MAPPER.dishToDishDto(dish);
        dishDto.setCategory(dishCategory);

        if (oldDishIngredientList != null) {
            List<DishIngredientDto> dishIngredientListDto = new ArrayList<>();

            for (DishIngredient dishIngredient : oldDishIngredientList) {
                dishIngredient.setIngredient(ingredientById(dishIngredient.getIngredient().getId()));

                IngredientDto ingredientDto =
                        IngredientMapper.MAPPER.ingredientToIngrdientDto(dishIngredient.getIngredient());
                DishIngredient newDishIngredient = new DishIngredient(
                        dish,
                        dishIngredient.getIngredient(),
                        dishIngredient.getQuantity()
                );

                DishIngredientDto newDishIngredientDto = DishIngredientMapper.MAPPER.dishIngredientToDishIngredientDto(
                        dishIngredientRepository.save(newDishIngredient)
                );
                newDishIngredientDto.setIngredient(ingredientDto);
                dishIngredientListDto.add(
                        newDishIngredientDto
                );
            }
            dishDto.setDishIngredients(dishIngredientListDto);
        }

        return dishDto;
    }

    private void deleteOldDishIngredients(Dish dish) {
        List<DishIngredient> oldDishIngredients = dishIngredientRepository.findByDishId(dish.getId());
        for (DishIngredient dishIngredient : oldDishIngredients) {
            dishIngredientRepository.deleteById(dishIngredient.getId());
        }
    }

    private Ingredient ingredientById(long dishIngredientId) {
        Ingredient ingredient = new Ingredient();
        Optional<Ingredient> ingredientOptional = ingredientRepository.findById(dishIngredientId);
        if (ingredientOptional.isPresent()) {
            ingredient = ingredientOptional.get();
        }
        return ingredient;
    }

    public List<DishDto> findAll() {
        return DishMapper.MAPPER.dishToDishDto(dishRepository.findAll());
    }

    public List<DishDto> findAllLimitedBy(int elements) {
        return DishMapper.MAPPER.dishToDishDto(
                dishRepository.findAll(PageRequest.of(0, elements)).getContent()
        );
    }

    public DishDto findById(long id) {
        Optional<Dish> dish = dishRepository.findById(id);
        return dish.map(DishMapper.MAPPER::dishToDishDto).orElse(null);
    }

    public List<DishDto> findAllFromCategory(String dishCategory) {
        DishCategory category = dishCategoryRepository.findByName(dishCategory);
        return DishMapper.MAPPER.dishToDishDto(
                dishRepository.findByCategory(category)
        );
    }

    public void delete(long dishId) {
        Optional<Dish> dishOptional = dishRepository.findById(dishId);
        if (dishOptional.isPresent()) {
            dishRepository.deleteById(dishId);
        } else {
            throw new ObjectNotFoundException("Dish id was not found. Id is incorrect");
        }
    }
}
