package pl.folusz.sebastian.app.service;

import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.DTO.DishFromOrderDto;
import pl.folusz.sebastian.app.DTO.DishOrderDto;
import pl.folusz.sebastian.app.DTO.OrderDto;
import pl.folusz.sebastian.app.Mapper.DishFromOrderMapper;
import pl.folusz.sebastian.app.Mapper.DishOrderMapper;
import pl.folusz.sebastian.app.Mapper.OrderMapper;
import pl.folusz.sebastian.app.dao.dish.DishRepository;
import pl.folusz.sebastian.app.dao.order.DishOrderRepository;
import pl.folusz.sebastian.app.dao.order.OrderRepository;
import pl.folusz.sebastian.app.dao.order.OrderStatusRepository;
import pl.folusz.sebastian.app.dao.reservation.ReservationRepository;
import pl.folusz.sebastian.app.entity.dish.Dish;
import pl.folusz.sebastian.app.entity.order.DishOrder;
import pl.folusz.sebastian.app.entity.order.Order;
import pl.folusz.sebastian.app.entity.order.OrderStatus;
import pl.folusz.sebastian.app.entity.reservation.Reservation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    private OrderRepository orderRepository;
    private DishOrderRepository dishOrderRepository;
    private OrderStatusRepository orderStatusRepository;
    private DishRepository dishRepository;
    private ReservationRepository reservationRepository;

    public OrderService(OrderRepository orderRepository, DishOrderRepository dishOrderRepository, OrderStatusRepository orderStatusRepository, DishRepository dishRepository, ReservationRepository reservationRepository) {
        this.orderRepository = orderRepository;
        this.dishOrderRepository = dishOrderRepository;
        this.orderStatusRepository = orderStatusRepository;
        this.dishRepository = dishRepository;
        this.reservationRepository = reservationRepository;
    }

    public OrderDto save(Order order) {
        List<DishOrder> dishOrders = new ArrayList<>(order.getDishOrders());
        List<DishOrderDto> newDishOrders = new ArrayList<>();
        order.getDishOrders().clear();
        order.setId(0);
        order.setDateTime(new Date());
        order.setStatus(
                saveOrUpdateStatus(order.getStatus())
        );
        order.setReservation(
                findReservation(order.getReservation().getId())
        );

        order = orderRepository.save(order);

        for (DishOrder dishOrder : dishOrders) {
            DishOrder savedDishOrder = saveDishOrder(dishOrder, order);
            DishFromOrderDto dishDto = DishFromOrderMapper.MAPPER.dishTodishFromOrderDto(
                    savedDishOrder.getDish()
            );
            DishOrderDto savedDishOrderDto = DishOrderMapper.MAPPER.dishOrderToDishOrderDto(
                    savedDishOrder
            );
            savedDishOrderDto.setDish(dishDto);
            newDishOrders.add(savedDishOrderDto);
        }
        OrderDto orderDto = OrderMapper.MAPPER.orderToOrderDto(order);
        orderDto.setDishOrders(newDishOrders);
        return orderDto;
    }

    public List<OrderDto> findAllNotPreparedFromToday() {
        return OrderMapper.MAPPER.orderToOrderDto(
                this.orderRepository.findAllNotPreparedFromToday()
        );
    }

    public List<OrderDto> findAll() {
        return OrderMapper.MAPPER.orderToOrderDto(
                this.orderRepository.findAll()
        );
    }

    public OrderDto findById(Long orderId) {
        Optional<Order> byId = this.orderRepository.findById(orderId);
        return byId.map(OrderMapper.MAPPER::orderToOrderDto).orElse(null);
    }

    private Dish findDish(long dishId) {
        Optional<Dish> dishById = dishRepository.findById(dishId);
        return dishById.orElse(null);
    }

    private DishOrder saveDishOrder(DishOrder dishOrder, Order order) {
        dishOrder.setId(0);
        dishOrder.setDish(
                findDish(dishOrder.getDish().getId())
        );
        dishOrder.setOrder(order);
        return dishOrderRepository.save(dishOrder);
    }

    private OrderStatus saveOrUpdateStatus(OrderStatus status) {
        if (orderStatusRepository.existsByName(status.getName())) {
            status = orderStatusRepository.save(
                    orderStatusRepository.findByName(status.getName())
            );
        } else {
            status = orderStatusRepository.save(status);
        }
        return status;
    }

    private Reservation findReservation(long reservationId) {
        Optional<Reservation> reservation = reservationRepository.findById(reservationId);
        return reservation.orElse(null);
    }

    public OrderDto updateStatus(Order newOrder, long orderId) {

        Optional<Order> byId = this.orderRepository.findById(orderId);
        if (!byId.isPresent()) {
            return null;
        } else {
            Order order = byId.get();
            order.setStatus(
                    saveOrUpdateStatus(newOrder.getStatus())
            );
            return OrderMapper.MAPPER.orderToOrderDto(
                    this.orderRepository.save(order)
            );
        }
    }
}
