package pl.folusz.sebastian.app.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.DTO.DishOrderDto2;
import pl.folusz.sebastian.app.Mapper.DishOrder2Mapper;
import pl.folusz.sebastian.app.dao.order.DishOrderRepository;

import java.util.List;

@Service
public class DishOrderService {
    private DishOrderRepository dishOrderRepository;

    public DishOrderService(DishOrderRepository dishOrderRepository) {
        this.dishOrderRepository = dishOrderRepository;
    }

    public List<DishOrderDto2> findAllByUser(String userEmail, Integer elements) {
        if (elements != null) {
            return DishOrder2Mapper.MAPPER.dishOrderToDto(
                    dishOrderRepository.findByUserAndLimitBy(userEmail, PageRequest.of(0, elements))
            );
        }
        return DishOrder2Mapper.MAPPER.dishOrderToDto(
                dishOrderRepository.findByUser(userEmail)
        );
    }

    public List<DishOrderDto2> findAll(Integer elements) {
        if (elements != null) {
            return DishOrder2Mapper.MAPPER.dishOrderToDto(
                    dishOrderRepository.findAll(PageRequest.of(0, elements)).getContent()
            );
        }
        return DishOrder2Mapper.MAPPER.dishOrderToDto(
                dishOrderRepository.findAll()
        );
    }
}
