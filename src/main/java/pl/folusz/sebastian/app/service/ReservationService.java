package pl.folusz.sebastian.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.DTO.RestaurantTableDto;
import pl.folusz.sebastian.app.Mapper.ReservationMapper;
import pl.folusz.sebastian.app.Mapper.RestaurantTableMapper;
import pl.folusz.sebastian.app.dao.reservation.ReservationRepository;
import pl.folusz.sebastian.app.DTO.ReservationDto;
import pl.folusz.sebastian.app.dao.restaurantTable.RestaurantTableRepository;
import pl.folusz.sebastian.app.dao.user.UserRepository;
import pl.folusz.sebastian.app.entity.reservation.Reservation;
import pl.folusz.sebastian.app.entity.restaurantTable.RestaurantTable;
import pl.folusz.sebastian.app.entity.user.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationService {
    private ReservationRepository reservationRepository;
    private UserRepository userRepository;
    private RestaurantTableRepository restaurantTableRepository;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository, UserRepository userRepository, RestaurantTableRepository restaurantTableRepository) {
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
        this.restaurantTableRepository = restaurantTableRepository;
    }

    public List<ReservationDto> findAllWithoutUser() {
        return convertRestaurantTablesToDto(this.reservationRepository.findAll());
    }

    public List<ReservationDto> findAllByUser(String userEmail) {
        return convertRestaurantTablesToDto(this.reservationRepository.findAllByUserEmailFromToday(userEmail));
    }

    public ReservationDto findByUser(long reservationId, String userEmail) {
        ReservationDto reservationDto = convertRestaurantTableToDto(this.reservationRepository.findByIdAndUserEmail(reservationId, userEmail));
        System.out.println("reservation date: " + reservationDto.getDate().toString());
        return reservationDto;
    }

    public ReservationDto save(Reservation reservation, String userEmail) {
        User user = this.userRepository.findByEmail(userEmail);
        Optional<RestaurantTable> table = this.restaurantTableRepository.findById(reservation.getTable().getId());
        reservation.setId(0);
        reservation.setUser(user);
        table.ifPresent(reservation::setTable);
        return convertRestaurantTableToDto(reservationRepository.save(reservation));
    }

    private List<ReservationDto> convertRestaurantTablesToDto(List<Reservation> reservations) {
        List<ReservationDto> reservationsDto = new ArrayList<>();
        for (Reservation reservation : reservations) {
            ReservationDto reservationDto = convertRestaurantTableToDto(reservation);
            reservationsDto.add(reservationDto);
        }
        return reservationsDto;
    }

    private ReservationDto convertRestaurantTableToDto(Reservation reservation) {
        RestaurantTableDto tableDto = RestaurantTableMapper.MAPPER.restaurantTableToRestaurantTableDto(reservation.getTable());
        ReservationDto reservationDto = ReservationMapper.MAPPER.reservationToReservationDto(reservation);
        reservationDto.setTable(tableDto);
        return reservationDto;
    }
}
