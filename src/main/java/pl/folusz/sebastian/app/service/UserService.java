package pl.folusz.sebastian.app.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.DTO.UserDto;
import pl.folusz.sebastian.app.Mapper.UserMapper;
import pl.folusz.sebastian.app.dao.user.RoleRepository;
import pl.folusz.sebastian.app.dao.user.UserRepository;
import pl.folusz.sebastian.app.entity.user.User;
import pl.folusz.sebastian.app.entity.user.UserRole;


@Service
public class UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public UserDto findByEmail(String email) {
        return UserMapper.MAPPER.userToUserDto(
                userRepository.findByEmail(email)
        );
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public User save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(1);
        UserRole userRole = roleRepository.findByRole("ADMIN");
        user.add(userRole);
        return userRepository.save(user);
    }

}
