package pl.folusz.sebastian.app.service;

import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.dao.dish.DishIngredientRepository;
import pl.folusz.sebastian.app.entity.dish.DishIngredient;

@Service
public class DishIngredientService {
    private DishIngredientRepository repository;

    public DishIngredientService(DishIngredientRepository repository) {
        this.repository = repository;
    }

    public DishIngredient save(DishIngredient dishIngredient) {
        return repository.save(dishIngredient);
    }
}
