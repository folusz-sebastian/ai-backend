package pl.folusz.sebastian.app.service;

import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.DTO.RestaurantTableDto;
import pl.folusz.sebastian.app.Mapper.RestaurantTableMapper;
import pl.folusz.sebastian.app.dao.restaurantTable.RestaurantTableRepository;
import pl.folusz.sebastian.app.dao.restaurantTable.PositionRepository;
import pl.folusz.sebastian.app.entity.restaurantTable.RestaurantTable;
import pl.folusz.sebastian.app.entity.restaurantTable.Position;
import pl.folusz.sebastian.app.exceptions.ObjectNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RestaurantTableService {
    private RestaurantTableRepository restaurantTableRepository;
    private PositionRepository positionRepository;

    public RestaurantTableService(RestaurantTableRepository restaurantTableRepository, PositionRepository positionRepository) {
        this.restaurantTableRepository = restaurantTableRepository;
        this.positionRepository = positionRepository;
    }

    public List<RestaurantTableDto> update(List<RestaurantTable> restaurantTables) {
        List<RestaurantTable> returnedRestaurantTables = new ArrayList<>();
        restaurantTables.forEach(restaurantTable -> {
            Position position = positionRepository.findByXInPercentsAndYInPercents(
                    restaurantTable.getPosition().getXInPercents(),
                    restaurantTable.getPosition().getYInPercents()
            );
            if (position == null) {
                position = positionRepository.save(new Position(
                        restaurantTable.getPosition().getXInPercents(),
                        restaurantTable.getPosition().getYInPercents()
                ));
            }
            restaurantTable.setPosition(position);
            returnedRestaurantTables.add(restaurantTableRepository.save(restaurantTable));
        });
        return RestaurantTableMapper.MAPPER.restaurantTableToRestaurantTableDto(returnedRestaurantTables);
    }

    public List<RestaurantTableDto> findAll() {
        return RestaurantTableMapper.MAPPER.restaurantTableToRestaurantTableDto(restaurantTableRepository.findAll());
    }

    public void delete(long id) {
        Optional<RestaurantTable> restaurantTableOptional = restaurantTableRepository.findById(id);
        restaurantTableOptional.ifPresent(restaurantTable -> restaurantTableRepository.delete(restaurantTable));
        if (restaurantTableOptional.isPresent()) {
            restaurantTableRepository.delete(restaurantTableOptional.get());
        } else {
            throw new ObjectNotFoundException("Table id was not found. Id is incorrect");
        }
    }

    public List<RestaurantTableDto> findAllFree(String year, String month, String day, int beginningHour, int duration) {
        return RestaurantTableMapper.MAPPER.restaurantTableToRestaurantTableDto(
                this.restaurantTableRepository.findAllFree(year, month, day, beginningHour, duration)
        );
    }
}
