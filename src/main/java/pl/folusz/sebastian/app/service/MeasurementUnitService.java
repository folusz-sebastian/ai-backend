package pl.folusz.sebastian.app.service;

import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.dao.dish.MeasurementUnitRepository;
import pl.folusz.sebastian.app.entity.dish.MeasurementUnit;

import java.util.List;

@Service
public class MeasurementUnitService {
    private MeasurementUnitRepository measurementUnitRepository;

    public MeasurementUnitService(MeasurementUnitRepository measurementUnitRepository) {
        this.measurementUnitRepository = measurementUnitRepository;
    }

    public List<MeasurementUnit> findAll() {
        return this.measurementUnitRepository.findAll();
    }
}
