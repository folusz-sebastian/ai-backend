package pl.folusz.sebastian.app.service;

import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.DTO.IngredientDto;
import pl.folusz.sebastian.app.Mapper.IngredientMapper;
import pl.folusz.sebastian.app.dao.dish.IngredientRepository;
import pl.folusz.sebastian.app.dao.dish.MeasurementUnitRepository;
import pl.folusz.sebastian.app.entity.dish.Ingredient;
import pl.folusz.sebastian.app.entity.dish.MeasurementUnit;
import pl.folusz.sebastian.app.exceptions.ObjectNotFoundException;

import java.util.List;
import java.util.Optional;

@Service
public class IngredientService {
    private IngredientRepository ingredientRepository;
    private MeasurementUnitRepository measurementUnitRepository;

    public IngredientService(IngredientRepository ingredientRepository, MeasurementUnitRepository measurementUnitRepository) {
        this.ingredientRepository = ingredientRepository;
        this.measurementUnitRepository = measurementUnitRepository;
    }

    public Ingredient save(Ingredient ingredient) {
        ingredient.setId(0);
        Optional<MeasurementUnit> measurementUnitOptional = this.measurementUnitRepository.findById(ingredient.getMeasurementUnit().getId());
        if (measurementUnitOptional.isPresent()) {
            ingredient.setMeasurementUnit(measurementUnitOptional.get());
        } else {
            throw new ObjectNotFoundException("MeasurementUnit was not found. Id is not correct");
        }
        return this.ingredientRepository.save(ingredient);
    }

    public List<IngredientDto> findAll() {
        return IngredientMapper.MAPPER.ingredientToIngrdientDto(
                this.ingredientRepository.findAll()
        );
    }

    public void delete(long id) {
        this.ingredientRepository.deleteById(id);
    }
}
