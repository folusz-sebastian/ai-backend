package pl.folusz.sebastian.app.Mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.folusz.sebastian.app.DTO.DishOrderDto;
import pl.folusz.sebastian.app.entity.order.DishOrder;

import java.util.List;

@Mapper
public interface DishOrderMapper {
    DishOrderMapper MAPPER = Mappers.getMapper(DishOrderMapper.class);

    DishOrderDto dishOrderToDishOrderDto(DishOrder dishOrder);
    List<DishOrderDto> dishOrderToDishOrderDto(List<DishOrder> dishOrder);
}
