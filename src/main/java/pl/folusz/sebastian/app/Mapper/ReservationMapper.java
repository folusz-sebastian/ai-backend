package pl.folusz.sebastian.app.Mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.folusz.sebastian.app.DTO.ReservationDto;
import pl.folusz.sebastian.app.entity.reservation.Reservation;

import java.util.List;

@Mapper
public interface ReservationMapper {
    ReservationMapper MAPPER = Mappers.getMapper(ReservationMapper.class);

    ReservationDto reservationToReservationDto(Reservation reservation);
    List<ReservationDto> reservationToReservationDto(List<Reservation> reservation);
}
