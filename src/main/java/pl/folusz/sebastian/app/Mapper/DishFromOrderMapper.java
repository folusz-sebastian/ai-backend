package pl.folusz.sebastian.app.Mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.folusz.sebastian.app.DTO.DishFromOrderDto;
import pl.folusz.sebastian.app.entity.dish.Dish;

import java.util.List;

@Mapper
public interface DishFromOrderMapper {
    DishFromOrderMapper MAPPER = Mappers.getMapper(DishFromOrderMapper.class);

    DishFromOrderDto dishTodishFromOrderDto(Dish dish);
    List<DishFromOrderDto> dishTodishFromOrderDto(List<Dish> dish);
}
