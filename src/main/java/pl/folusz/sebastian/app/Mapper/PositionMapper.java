package pl.folusz.sebastian.app.Mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.folusz.sebastian.app.DTO.PositionDto;
import pl.folusz.sebastian.app.entity.restaurantTable.Position;

@Mapper
public interface PositionMapper {
    PositionMapper MAPPER = Mappers.getMapper(PositionMapper.class);

    PositionDto positionToPositionDto(Position position);
}
