package pl.folusz.sebastian.app.Mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.folusz.sebastian.app.DTO.DishOrderDto2;
import pl.folusz.sebastian.app.entity.order.DishOrder;

import java.util.List;

@Mapper
public interface DishOrder2Mapper {
    DishOrder2Mapper MAPPER = Mappers.getMapper(DishOrder2Mapper.class);

    DishOrderDto2 dishOrderToDto(DishOrder dishOrder);
    List<DishOrderDto2> dishOrderToDto(List<DishOrder> dishOrder);
}
