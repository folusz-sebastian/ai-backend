package pl.folusz.sebastian.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import pl.folusz.sebastian.app.DTO.DishOrderDto2;
import pl.folusz.sebastian.app.DTO.OrderDto;
import pl.folusz.sebastian.app.entity.order.Order;
import pl.folusz.sebastian.app.service.DishOrderService;
import pl.folusz.sebastian.app.service.OrderService;

import java.util.List;

@RestController
@RequestMapping("api")
public class OrderController {
    private OrderService orderService;
    private DishOrderService dishOrderService;

    public OrderController(OrderService orderService, DishOrderService dishOrderService) {
        this.orderService = orderService;
        this.dishOrderService = dishOrderService;
    }

    @GetMapping("/orders")
    public ResponseEntity<List<OrderDto>> findAll(@RequestParam(required = false, name = "fromToday") boolean fromToday) {
        return fromToday ?
                ResponseEntity.ok(this.orderService.findAllNotPreparedFromToday()) :
                ResponseEntity.ok(this.orderService.findAll());
    }

    @GetMapping("/orders/{orderId}")
    public ResponseEntity<OrderDto> findById(@PathVariable long orderId) {
        return ResponseEntity.ok(this.orderService.findById(orderId));
    }

    @PostMapping("/orders")
    @ResponseBody
    public ResponseEntity<OrderDto> save(@RequestBody Order order) {
        return ResponseEntity.ok(orderService.save(order));
    }

    @PutMapping("/orders/{orderId}")
    public ResponseEntity<?> update(@RequestBody Order order, @PathVariable long orderId) {
        OrderDto orderDto = orderService.updateStatus(order, orderId);
        if (orderDto == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(orderDto);
        }
    }

    @GetMapping("/ordered_dishes")
    public List<DishOrderDto2> findAllOrderedDishes(@RequestParam(name = "by_user") boolean byUser,
                                                    @RequestParam(required = false) Integer elements,
                                                    Authentication authentication) {
        return byUser ?
                this.dishOrderService.findAllByUser(authentication.getName(), elements) :
                this.dishOrderService.findAll(elements);
    }
}
