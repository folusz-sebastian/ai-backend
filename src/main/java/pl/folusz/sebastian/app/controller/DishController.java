package pl.folusz.sebastian.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.folusz.sebastian.app.DTO.DishDto;
import pl.folusz.sebastian.app.entity.dish.Dish;
import pl.folusz.sebastian.app.entity.message.ResponseMessage;
import pl.folusz.sebastian.app.service.DishService;
import java.util.List;

@RestController
@RequestMapping("api")
public class DishController {
    private DishService dishService;

    public DishController(DishService dishService) {
        this.dishService = dishService;
    }

    @PostMapping("/dishes")
    @ResponseBody
    public ResponseEntity<DishDto> save(@RequestBody Dish dish) {
        return ResponseEntity.ok(dishService.save(dish));
    }

    @PutMapping("/dishes/{dishId}")
    @ResponseBody
    public ResponseEntity<DishDto> update(@PathVariable long dishId, @RequestBody Dish dish) {
        return ResponseEntity.ok(dishService.update(dishId, dish));
    }

    @GetMapping("/dishes")
    @ResponseBody
    public ResponseEntity<List<DishDto>> findAll(@RequestParam(required = false) Integer elements,
                                 @RequestParam(required = false, name = "category") String dishCategory) {
        if (elements != null) {
            return ResponseEntity.ok(dishService.findAllLimitedBy(elements));
        } else if (dishCategory != null) {
            return ResponseEntity.ok(dishService.findAllFromCategory(dishCategory));
        }
        return ResponseEntity.ok(dishService.findAll());
    }

    @GetMapping("/dishes/{dishId}")
    @ResponseBody
    public ResponseEntity<DishDto> findAll(@PathVariable long dishId) {
        return ResponseEntity.ok(dishService.findById(dishId));
    }

    @DeleteMapping("/dishes/{dishId}")
    @ResponseBody
    public ResponseEntity<ResponseMessage> delete(@PathVariable long dishId) {
        dishService.delete(dishId);
        return ResponseEntity.ok(new ResponseMessage("dish was deleted successfully"));
    }
}
