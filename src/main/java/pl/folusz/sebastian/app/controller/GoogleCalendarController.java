package pl.folusz.sebastian.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.folusz.sebastian.app.entity.message.ResponseMessage;
import pl.folusz.sebastian.app.service.GoogleCalendarService;
import java.io.IOException;

@RestController
@RequestMapping("api")
public class GoogleCalendarController {
    private GoogleCalendarService service;

    public GoogleCalendarController(GoogleCalendarService service) {
        this.service = service;
    }

    @GetMapping("/login_to_google")
    public ResponseEntity<ResponseMessage> redirectToLoginInGoogle() throws Exception {
        return ResponseEntity.ok(new ResponseMessage(service.authorize()));
    }

    @GetMapping("/calendar_event")
    public ResponseEntity<ResponseMessage> addEvent(@RequestParam(value = "google_cal_code") String code,
                                      @RequestParam(name = "start_date_time") String startDateTime,
                                      @RequestParam(name = "end_date_time") String endDateTime,
                                      @RequestParam(name = "title") String title
    ) {
        ResponseMessage responseMessage = new ResponseMessage("Nie udało się dodać wydarzenia do kalendarza");
        service.buildApiClientService(code);
        try {
            service.addEvent(startDateTime, endDateTime, title);
            responseMessage.setMessage("Wydarzenie zostało pomyślnie dodane");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(responseMessage);
    }
}
