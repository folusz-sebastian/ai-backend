package pl.folusz.sebastian.app.entity.dish;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@Getter @Setter
@Entity
@Table(name = "ingredient")
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ingredient_id")
    private long id;

    @Column(name = "name")
    private String name;

    @JsonProperty("measurement_unit")
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "measurement_unit_id")
    private MeasurementUnit measurementUnit;

    @OneToMany(mappedBy = "ingredient", cascade = CascadeType.ALL)
    private List<DishIngredient>  dishIngredients;

}
