package pl.folusz.sebastian.app.entity.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import pl.folusz.sebastian.app.entity.reservation.Reservation;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "restaurant_order")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private long id;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "reservation_id")
    private Reservation reservation;

    //    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(name = "date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "status_id")
    private OrderStatus status;

    @Column(name = "cost")
    @Digits(integer = 5, fraction = 2)
    private double cost;

    @JsonProperty("dish_orders")
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private List<DishOrder> dishOrders;
}
