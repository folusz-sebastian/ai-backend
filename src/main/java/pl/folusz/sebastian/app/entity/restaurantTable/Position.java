package pl.folusz.sebastian.app.entity.restaurantTable;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Getter @Setter
@Entity
@Table(name = "position")
public class Position {
    public Position(double xInPercents, double yInPercents) {
        this.xInPercents = xInPercents;
        this.yInPercents = yInPercents;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "position_id")
    private long id;

    @Column(name = "x_in_percents")
    @JsonProperty("x_in_percents")
    private double xInPercents;

    @Column(name = "y_in_percents")
    @JsonProperty("y_in_percents")
    private double yInPercents;
}
