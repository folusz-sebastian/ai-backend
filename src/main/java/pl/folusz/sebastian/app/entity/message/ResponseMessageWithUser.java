package pl.folusz.sebastian.app.entity.message;

import lombok.Getter;
import lombok.Setter;
import pl.folusz.sebastian.app.entity.user.User;

@Getter @Setter
public class ResponseMessageWithUser {
    private String message;
    private User user;

    public ResponseMessageWithUser(String message, User user) {
        this.message = message;
        this.user = user;
    }
}
