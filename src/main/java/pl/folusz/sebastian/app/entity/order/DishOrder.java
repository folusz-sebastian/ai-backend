package pl.folusz.sebastian.app.entity.order;

import lombok.Getter;
import lombok.Setter;
import pl.folusz.sebastian.app.entity.dish.Dish;

import javax.persistence.*;

@Getter @Setter
@Entity
@Table(name = "dish_restaurant_order")
public class DishOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dish_order_id")
    private long id;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "dish_id")
    private Dish dish;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "order_id")
    private Order order;

    @Column(name = "quantity")
    private int quantity;
}
